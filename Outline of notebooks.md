---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Outline

* **Notebook 1:** Basics of programming and Python
* **Notebook 2:** Functions
* **Notebook 3:** Program flow control
* **Notebook 4:** Scientific computing with numpy
* **Notebook 5:** Data in Python
