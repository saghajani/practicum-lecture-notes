{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions in Python\n",
    "\n",
    "In this notebook, we will explore the implementation of functions in Python. \n",
    "\n",
    "**Learning objectives for this notebook:**\n",
    "\n",
    "* Student is able to define functions with input parameters to execute a piece of code (use functions as \"methods\")\n",
    "* Student is able to create and use functions that return a value (or multiple values)\n",
    "* Student is able to import functions from libraries / modules\n",
    "* Student is able to use Shift-Tab to bring up the help for a function from a library\n",
    "* Student is able to predict if a variable name in a function refers to a local variable or a global variable\n",
    "\n",
    "## Functions to save typing\n",
    "\n",
    "In programming, you often want to repeat the same sequence of commands over and over again. \n",
    "\n",
    "One way to do this is to copy and paste the same piece of code over and over again. This is actually quite easy, but runs quickly into a problem: let's say you want to change a little bit what that code will do, then you need to change it in many places. If you change it in one place but forget in another, then your program might crash (ie. give an error). Or even worse, and even harder to <a href=https://en.wikipedia.org/wiki/Debugging>debug</a> the mistake may  may give an error message but give you the wrong answer!\n",
    "\n",
    "For this reason (among others), programming languages allow programmers to define \"functions\". Functions are pieces of code that you can give a name and then enable you to them use over and over again, without having to retype the code text. \n",
    "\n",
    "As an example, let's say that we want to print out the value of a variables named `a` and `b` using a long sentence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = 6\n",
    "b = 4\n",
    "print(\"The value of variable a is\", a)\n",
    "print(\"The value of variable b is\", b)\n",
    "\n",
    "a = a/2\n",
    "b = 3\n",
    "print(\"The value of variable a is\", a)\n",
    "print(\"The value of variable b is\", b)\n",
    "\n",
    "a = a+1\n",
    "b = 1.5\n",
    "print(\"The value of variable a is\", a)\n",
    "print(\"The value of variable b is\", b)\n",
    "\n",
    "a = a-20\n",
    "b = -1e4\n",
    "print(\"The value of variable a is\", a)\n",
    "print(\"The value of variable b is\", b)\n",
    "\n",
    "a = a+1j\n",
    "b = 1\n",
    "print(\"The value of variable a is\", a)\n",
    "print(\"The value of variable b is\", b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save a lot of typing, one can define a simple function to do this work for us. To define a function, you use the following syntax:\n",
    "\n",
    "```\n",
    "def function_name():\n",
    "     ...\n",
    "```\n",
    "\n",
    "Here, you replace the `...` with the code you want to function to execute. The Python the code inside the function should be <a  href=https://en.wikipedia.org/wiki/Indentation_(typesetting)>indented</a> by starting each line with a <a href=https://en.wikipedia.org/wiki/Tab_key>tab</a>. By default, adding a tab will produce 4 spaces in your code. You can also \"indent\" your code by manually adding spaces, but you must make sure to add 4 spaces each time. The Jupyter notebook will try to detect if you make a mistake in your indentation, and will sometimes color your text in red if it detects a mistake. \n",
    "\n",
    "Tabs in Python are VERY IMPORTANT: python uses tabs to know which code is inside the function and which is not. If you make a mistake with the tabs in such a way that python cannot understand what you mean, it will give you an `IdentationError`.\n",
    "\n",
    "In notebooks, you can also select a line, or multiple lines, and then use `Tab` to increase their indentation level, or use `Shift-Tab` to decrease their indentation level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_tab_and_shift_tab():\n",
    "    some code\n",
    "        that is indendented\n",
    "    try selecting this text\n",
    "    and then pushing tab \n",
    "    and shift-tab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have defined your function, you can execute it by using the code `function_name()`. \n",
    "\n",
    "Let's look at how to use a function as a \"procedure\" to simplify the code above: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def print_status():\n",
    "    print(\"The value of variable a is\", a)\n",
    "    print(\"The value of variable b is\", b)\n",
    "\n",
    "a = 6\n",
    "b = 4\n",
    "print_status()\n",
    "\n",
    "a = a/2\n",
    "b = 3\n",
    "print_status()\n",
    "\n",
    "a = a+1\n",
    "b = 1.5\n",
    "print_status()\n",
    "\n",
    "a = a-20\n",
    "b = -1e4\n",
    "print_status()\n",
    "\n",
    "a = a+1j\n",
    "b = 1\n",
    "print_status()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, it may not be such a big deal, but you can imagine that as the code in your function becomes more and more complicated, it will save you a lot of time. Also, imagine that I wanted to change the wording of the sentence I print: in the case with the function, I would only have to do this once, while in the example without function, I would have to manually change this at 5 different places. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.1** Write your own function that contains two lines of code. The first line should make a new variable `var2` that is `var` converted to an integer. The second line of your code should print the value of `var2`. \n",
    "\n",
    "Using this code, play around with the indentation (add extra tabs and spaces for example) to see how 'critical' Python is with indentation. For example: does three spaces work instead of `Tab`? Does one space work? What about `Tab` on the first line and three spaces on the second line? Can you make Python trigger an `IdentationError`? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "var=3.5\n",
    "\n",
    "# Your function here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions with input variables\n",
    "\n",
    "Let's say that we wanted to print out the status of variables that we do not know the name of ahead of time, as in the example above. Say we wanted to make a function that could print out a message with the status of value of ANY variable. How could we do this? \n",
    "\n",
    "In the example above, our function explicitly printed out variables `a` and `b`. But this only works because I know in advance that the person using my function has defined variables `a` and `b`. But what if I want to print the value of variable `c`? \n",
    "\n",
    "To allow functions to be more generic, and therefore more \"reusable\" in general, Python allows you to define \"input variables\" for your function. The syntax for this is the following:\n",
    "\n",
    "```\n",
    "def function_name(x):\n",
    "    ...\n",
    "```\n",
    "\n",
    "When you do this, for the code INSIDE your function, a variable `x` will be defined that will have the value given by the input value given to the function by the user. Let's look at a specific example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_status2(x):\n",
    "    print(\"The value passed to the function is\", x)\n",
    "\n",
    "a = 1.5\n",
    "print_status2(a)\n",
    "\n",
    "a = 1+1j\n",
    "print_status2(a)\n",
    "\n",
    "print_status2(1.5323)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How does this work? \n",
    "\n",
    "When the function `print_status(a)` is called, Python \"sends\" (\"passes\" in computer speak) the value of `a` to the function. Inside the function, Python creates a new (temporary) variable called `x`, that is defined ONLY while the function code is running. This temporary variable `x` is then assigned the value that was sent to the function, and then the code is executed. When the function is finished, the variable `x` is destroyed. (Try adding the code `print(x)` above outside the function and see what happens!)\n",
    "\n",
    "Note, as you can see in the third example, the things you pass to functions do not even need to be variables! This is fine because the function only needs the value of the argument that is passed to the function. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.2** Copy your code from exercise 2.1 (but then with just normal indentation using tabs) into the cell below and change it such that it uses a function with input parameters to achieve the same task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions with multiple inputs \n",
    "\n",
    "Functions can also take multiple input variables. To do this, you put them all in between the brackets `()`, separated by commas. For example, with 3 variables, the syntax is:\n",
    "\n",
    "```\n",
    "def function_name(variable1, variable2, variable3):\n",
    "    ...\n",
    "```\n",
    "\n",
    "You would then use this function in the following way:\n",
    "\n",
    "```\n",
    "function_name(argument1, argument2, argument3)\n",
    "```\n",
    "\n",
    "When you do this, inside the function, `variable1` will get assigned the value of `argument1`, `variable2` will get assigned the value of `argument2`, and `variable3` will get assigned the value of `argument3`. This matching of the position in the list is called matching by \"positional order\".  \n",
    "\n",
    "Note that there are several different names used for the \"input variables\" of a function: often, computer scientists will also use the name \"input arguments\" (or just \"arguements), or \"input parameters\" (or just \"parameters\"). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_status3(x, y):\n",
    "    print(\"The value of the first input variable is \", x)\n",
    "    print(\"The value of the second input variable is \", y)\n",
    "\n",
    "print_status3(1,2)\n",
    "print_status3(2.5,1.5)\n",
    "print_status3(a, 2*a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.3** Make a  new function `print_status4()` that takes three variables as arguments and prints out messages telling the user the values of each of them (as above, but with three input variables). Test it to make sure it works. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions that return a value\n",
    "\n",
    "In addition to receiving values as inputs, functions can also send back values to the person using the function. In computer programming, this is called the \"return value\". \n",
    "\n",
    "When you create a function, you can use the `return` command to specify what value should be sent back to the person using the function. Let's look at an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_formula(x):\n",
    "    y = x**2 + 3\n",
    "    return y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To \"capture\" the value returned by the function, you can assign it to a varible like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = my_formula(3.5)\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also just directly \"use\" the result of the function if you want:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(my_formula(4.6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that as soon as python sees the `return` command, it stops running the function, so any code after it will not be executed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def myfunction(x):\n",
    "    print(\"This gets printed.\")\n",
    "    return x**2 + 3\n",
    "    print(\"This does not.\")\n",
    "    \n",
    "print(myfunction(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to send back more than one result to the user of your function, you can separate the results with commas when you use the `return` command. How do you make use of these two variables that you send back? You will explore this in this exercise:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.4** **(a)**  Write a function that takes two real numbers as input and returns the sum and product of the two numbers. In your function, try to send *both* of the calculated numbers back as a return value. We have not taught you that yet so you will have to look it up: I recommend trying a google search for \"python function return two variables\". "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your function here\n",
    "def product_and_sum(...):\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(b)** Now USE your function to calculate the sum and product of `a` and `b`, \"capturing\" the sum and product in variables `s` and `p`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=1.5\n",
    "b=2.5\n",
    "\n",
    "...some code that uses the return value of your function to set variable s and p...\n",
    "\n",
    "print(\"Sum is:\", s)\n",
    "print(\"Product is:\", p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing functions from libraries\n",
    "\n",
    "One of the big advantages of python is that there are huge collection of libraries that include code for doing a huge number of things for you! We will make extensive use of the library `numpy` for numerical calculations in Python, and the library `matplotlib` for generating scientific plots. Beyond this, nearly anything you want to be able to do on a computer can be found in Python libraries, which is one of the reasons it is so popular. \n",
    "\n",
    "In order make use of these libraries of code, you need to \"import\" them into the \"namespace\" of your kernel. \n",
    "\n",
    "(\"Namespace\" is Python-speak for the list of functions and variable names that you can find in the running copy of python that is connected to your notebook.)\n",
    "\n",
    "Here, we will show you a few examples of different ways of importing code into your notebook from a library (also called a \"module\"). For this, we will take the example we used already in Notebook 1: in the module `time`, there is a function called `sleep()` that will perform the task of \"pausing\" for a number of seconds given by the its argument. \n",
    "\n",
    "You can find out more about the `time` module by looking at its documentation webpage:\n",
    "\n",
    "https://docs.python.org/3/library/time.html\n",
    "\n",
    "and specifically about the `sleep()` function here:\n",
    "\n",
    "https://docs.python.org/3/library/time.html#time.sleep\n",
    "\n",
    "### Importing a whole module\n",
    "\n",
    "The simplest way to be able use the `sleep`  function of the `time` module is to import it using the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see it has been imported by using the `%whos` command: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once it has been imported, you can access all the functions of the module by adding `time.` in front of the function name (from the time module) in your code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(\"Starting to sleep\")\n",
    "time.sleep(5)\n",
    "print(\"Done!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you import the whole module, you will have access to all the functions in it. To see what functions are in the module for you to use type `dir(sleep)`, which will generate this list.\n",
    "\n",
    "Sometimes, if you will be using the functions from the module a lot, you can give it a different \"prefix\" to save yourself some typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time as tm\n",
    "print(\"Starting to sleep\")\n",
    "tm.sleep(5)\n",
    "print(\"Done!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use this a lot when using the `numpy` module, shortening its name to `np` when we import it, and also for the `matplotlib.pyplot` submodule, which we will shorten to `plt`. (These are also typically used conventions in the scientific community.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Importing a single function \n",
    "\n",
    "If you need only a single function from a library, there is also a second commonly used way to import only that single function using the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import sleep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you do this, the function `sleep()` will be available directly in your notebook kernel \"namespace\" without any prefix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(\"Starting to sleep\")\n",
    "sleep(5)\n",
    "print(\"Done!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `%whos`, we can now see that we have three different ways to use the `sleep()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you look around on the internet, you will also find people that will do the following\n",
    "\n",
    "```\n",
    "from numpy import *\n",
    "```\n",
    "\n",
    "This will import all the functions from numpy directly into the namespace of your kernel with no prefix. You might think: what a great idea, this will save me loads of typing! Instead of typing `np.sqrt()` for example, to use the square-root function, I could just type `sqrt()`. \n",
    "\n",
    "While true, it will save typing, it also comes with a risk: sometimes different modules have functions that have the same name, but do different things. A concrete example is the function `sqrt()`, which is available in both the `math` module and the `numpy` module. Unfortunately, `math.sqrt()` will give an error when using numpy arrays (which we will learn more about in later notebooks). \n",
    "\n",
    "If you import both of them, you will overwrite these functions by the second import, and if you're not careful, you will forget which one you are using, and it could cause your code to break. It will also \"crowd\" your notebooks namespace: using the `whos` function, you will suddenly see hundreds or even thousands of functions, instead of only just a module. \n",
    "\n",
    "For these reasons, it is generally advised not to use `import *`, and it is considered poor coding practice in modern python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shift-Tab for getting help\n",
    "\n",
    "Like the tab completion we saw in the first notebook, Jupyter also can give you help on functions you have imported from libraries if you type `Shift-Tab`. \n",
    "\n",
    "Say I forgot how to use the `sleep()` function. If I type the word \"sleep\" and then push `Shift-Tab`, Jupyter will bring up a help window for that function.\n",
    "\n",
    "Try it: click on any part of the word `sleep` in the following code cell and push `Shift-Tab`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sleep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also find the same help as the output of a code cell by using the `help()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(sleep)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are extensive online resources for many modules. The most used modules have  helpful examples on the functions and how to implement them. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.5 (a)** Find help for the built-in functions `abs`, `int`, and `input`. Which of the help functions are easy to read? Which one does not provide such useful information (compared to the online documentation page)? (Put each help command in a separate cell)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(b)** Import the function `glob` from the library `glob` and print its help information. What does the function `glob(\"../*\")` do? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run the help here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Global variables, local variables, and variable scope\n",
    "\n",
    "In our first functions above, we saw a couple of examples of using variables inside functions. \n",
    "\n",
    "In the first example, we used the variables `a` and `b` inside our function that we created outside our function, directly in our notebook. \n",
    "\n",
    "In the second example, we used the \"temporary\" variable `x` inside our function. \n",
    "\n",
    "These were two examples of different variable \"scope\". In computer programming, <a href=https://en.wikipedia.org/wiki/Scope_(computer_science)>scope</a> define the rules Python uses when it tries to look up the value of a variable. \n",
    "\n",
    "In the slightly simplified picture we will work with here, variables can have two different types of \"scopes\": **global scope** and **local scope**. \n",
    "\n",
    "If Python looks for a variable value, it first looks in the local scope (also called \"local namespace\"). If it does not find it, python will go up into the global scope (also called the \"global namespace\") and look for the variable there. If it does not find the varible there, it will trigger an error (a `NameError` to be precise).\n",
    "\n",
    "How do I create a global variable? By default, if you create a variable directly in your notebook (and not in a function in your notebook), it will always be **global**. So, actually, you've already created a bunch of global variables! \n",
    "\n",
    "Any variables you define inside a function in your code will be a **local** variable (including the input variables automatically created if your function takes any arguments). \n",
    "\n",
    "If you want to create a global variable inside a function, or make sure the variable you are referring to is the global variable and not the local one, you can do this by the `global` qualifier, which we will look at in a minute. \n",
    "\n",
    "Let's take a look at this in more detail by analysing a few examples. \n",
    "\n",
    "**Example 1** Accessing a global variable inside a function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a1 = 5\n",
    "\n",
    "def my_func():\n",
    "    print(a1)\n",
    "    \n",
    "my_func()\n",
    "a1 = 6\n",
    "my_func()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, when Python is inside the function `my_func()`, it first looks to see if there is a variable `a1` in the local scope of the function. It does not find one, so it then goes and looks in the global scope. There, it finds a variable `a1`, and so it uses this one. \n",
    "\n",
    "\n",
    "**Example 2** An example that doesn't work (unless you've run the next cell, in which case it will only fail again after you restart your kernel)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def my_func():\n",
    "    print(b1)\n",
    "    \n",
    "my_func()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This code gives a `NameError` because there is no variable `b1` yet created in the global scope. If we run the following code cell and try the code above again, it will work. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b1 = 6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you can see one of risks of languages like python: because of the persistent memory of the kernel, code can succeed or fail depending on what code you have run before it...\n",
    "\n",
    "If you want to see the error message above again, you can delete variable b1 using this code and run it again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del b1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example 3** Variables defined in the local scope of a function are not accessible outside the function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def my_func():\n",
    "    x = 5\n",
    "    \n",
    "my_func()\n",
    "\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example 4** Variables passed to functions cannot be modified by the function (more on this later when we look at more complicated data structures...sometimes this is different)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def my_func(a):\n",
    "    a = 6\n",
    "    \n",
    "a=5\n",
    "my_func(a)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This one is a bit subtle (mega-confusing?) because we re-used the same name `a` for the local variable in the function as the global variable outside of the function. However, the operation is quite logical. When the function code starts running, it creates a `local` variable `a` to store the value it received. And now, because there is already a local variable called `a`, using `a` in the function refers to the `local` variable `a`, not the `global` variable `a` we define before calling the function. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example 5** This one is a tricky one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 6\n",
    "\n",
    "def my_func():\n",
    "    a = 7\n",
    "\n",
    "print(a)\n",
    "my_func()\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " It would seem that the function would refer to the global variable `a` and therefore change it's value. However, it is tricky since we first use `a` in the function in an assignment. An assignment in python will automatically create a variable if it does not exist, and so python creates a new variable named `a` in the local scope. The name `a` inside the function now refers to this newly created local variable, and therefore the global variable will not be changed. In fact, this guarantees that you cannot change global variables inside a function, unless you use the `global` qualifier shown in the next example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example 6** If you want to make sure that the `a` inside your function is referring to the global variable `a`, you can include the line `global a` inside your function to tell python that you mean the global variable `a`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 6\n",
    "\n",
    "def my_func():\n",
    "    global a\n",
    "    a = 7\n",
    "\n",
    "print(a)\n",
    "my_func()\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in general, it is considered bad programming practice to use (too many) global variables. Why? When you write longer and bigger sections of code, it is easier to understand what is going in in your function if your function uses only local variables and communicates back and forth using input parameter and return variables. Using too many global variables in a function can be confusing because they are defined in a different place in your code and so you don't have a good oversight of them. (Bigger projects can easily have  10,000+ lines of code!)  \n",
    "\n",
    "In computer science, this is a topic of often intense debate (resulting in what nerds refer to as a <a href=\"https://www.urbandictionary.com/define.php?term=flame%20war\">flame war</a>), with global variables being branded as \"dangerous\" like in this stack exchange post:  \n",
    "\n",
    "https://stackoverflow.com/questions/423379/using-global-variables-in-a-function\n",
    "\n",
    "But I personally agree with the comments in this post that \"global variables have their place but should be used sparingly\".\n",
    "\n",
    "Summary of the rules for global and local variables:\n",
    "\n",
    "* If a local variable of the same name exists or is created by python (by assignment, for example), then python uses the local varible\n",
    "* If you try to use a variable name that does not exist locally, python checks for a global variable of the same name\n",
    "* If you want to change the value of a global inside a function, then you must use the `global` statement to make it clear to python than you want that name to refer to the global variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solutions to Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.1** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "var=3.5\n",
    "\n",
    "# Example: one  space is actually enough! But is discouraged, as you can see\n",
    "# by the fact the the notebook made it red colored. \n",
    "def myfunction():\n",
    " var2 =  int(var)\n",
    " print(var2)\n",
    "\n",
    "# Once you've started a code block though with a specific indentaton, then you cant change\n",
    "# it anymore.\n",
    "def myfunction():\n",
    "  var2 =  int(var)\n",
    " print(var2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.2** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def myfunction(var):\n",
    "    var2 =  int(var)\n",
    "    print(var2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.3** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def print_status3(x, y, z):\n",
    "    print(\"The value of the first input variable is \", x)\n",
    "    print(\"The value of the second input variable is \", y)\n",
    "    print(\"The value of the second input variable is \", z)\n",
    "\n",
    "print(1,2,3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.4** **(a)** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your function here\n",
    "def product_and_sum(a,b):\n",
    "    return a*b, a+b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(b)** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=1.5\n",
    "b=2.5\n",
    "\n",
    "p,s = product_and_sum(a,b)\n",
    "\n",
    "print(\"Sum is:\", s)\n",
    "print(\"Product is:\", p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2.5** **(a)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(abs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(int)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "help(input)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(b)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from glob import glob\n",
    "help(glob)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "glob(\"../*\")\n",
    "# It returns list of files and folders in the parent directory of that\n",
    "# in which this notebook is stored"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": "2",
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "430.594px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
